<?php defined('BASEPATH') OR exit('No direct script access allowed');

//use Elasticsearch\ClientBuilder;
//require 'vendor/autoload.php';

/**
* 
*/
class Transactions extends CI_Model
{
	
	public function getTopup($limit = 1000, $start_time=null, $end_time = null, $searchParams = array() )
	{
		$et = new DateTime($end_time);
		$et->add( new DateInterval('P1D') );
		$end = $et->format('Y-m-d');

		set_time_limit ( 0 ); //Set script to run infinitely
		ini_set('memory_limit','128M');//Increase allowed memory limit

		$elk_url = 'http://192.168.168.42:10200/upp-prod-txn-*/transactions/_search?pretty=true&size=' . $limit;
		
		$elk_query = '
			{
				"query" : {
					"bool" : {
						"must":[
						{
							"match" : { "recipientId" : "dart" }
						}
						';

	    foreach($searchParams as $key=>$value) {
	    	if( $value != '*' and ! empty($value) ) {
	    		$elk_query .= ',{"match" : { "' . $key . '" : "' . $value . '" }}';
	    	}
		}

		$elk_query .= ']
					,"must_not": [{ "match": { "parameters.afcAction": "CheckIn" } }]
					}
				},
				"sort" : [ { "receivedAt" : { "order":"desc" } } ],
				"filter": {
					"bool": {
						"must": { "range": { "receivedAt": { "gte": "' . ($start_time ? $start_time : 'now-23d/d') . '" }}},'
						. ( $end_time ? ('"must": { "range": { "receivedAt": { "lte": "' . $end . '" }}},' ) : '') .
						'
						"must_not": { 
						    "term": { "state": "DUPLICATE" }
						 }
					}
					
				}
			}';
		//die($elk_query);
		$response = exchange_json($elk_url, $elk_query);

		$hits = $response['hits']['hits'];
		$data['rows'] = [];
		$data['searchParams'] = $searchParams;
		$data['date_from'] = $start_time;
		$data['date_to'] = $end_time;
		$data['total'] = $response['hits']['total'];

		foreach($hits as $hit)
		{
			$row = [];
			$row['id'] = $hit['_id'];
				$dt = new DateTime();
				$dt->setTimestamp( $hit['_source']['receivedAt']/1000 );
			$row['time'] = $dt;
			$row['processor'] = $hit['_source']['processorName'];
			$row['processorRequestId'] = $hit['_source']['processorRequestId'];
			$row['status'] = $hit['_source']['status'];
			$row['initiator'] = $hit['_source']['initiator'];
			$row['amount'] = $hit['_source']['normalizedAmount'];
			$row['notifiedAt'] = @$hit['notifiedAt'];//optionally present
			$row['trustAcntSettledAt'] = @$hit['parameters']['trustAcntSettledAt'];
			$row['trustAcntStlmntRefNo'] = @$hit['parameters']['trustAcntStlmntRefNo'];

			$data['rows'][] = $row;
		}

		return $data;
	}

	public function getUsage($limit = 1000, $start_time=null, $end_time = null, $searchParams)
	{
		$et = new DateTime($end_time);
		$et->add( new DateInterval('P1D') );
		$end = $et->format('Y-m-d');

		set_time_limit ( 0 ); //Set script to run infinitely
		ini_set('memory_limit','128M');//Increase allowed memory limit

		$elk_url = 'http://192.168.168.42:10200/upp-prod-txn-*/transactions/_search?pretty=true&size=' . $limit;
		
		$elk_query = '
			{
				"query" : {
					"bool" : {
						"must":[
						{
							"match" : { "recipientId" : "dart" }
						}
						,{
							"match" : { "parameters.afcAction" : "CheckIn" }
						}
						';

	    foreach($searchParams as $key=>$value) {
	    	if( $value != '*' and ! empty($value) ) {
	    		$elk_query .= ',{"match" : { "' . $key . '" : "' . $value . '" }}';
	    	}
		}

		$elk_query .= ']
					}
				},
				"sort" : [ { "receivedAt" : { "order":"desc" } } ],
				"filter": {
					"bool": {
						"must": { "range": { "receivedAt": { "gte": "' . ($start_time ? $start_time : 'now-23d/d') . '" }}},'
						. ( $end_time ? ('"must": { "range": { "receivedAt": { "lte": "' . $end . '" }}},' ) : '') .
						'"must_not": { 
						    "term": { "state": "DUPLICATE" }
						 }
					}
					
				}
			}';
		//die($elk_query);
		$response = exchange_json($elk_url, $elk_query);

		$hits = $response['hits']['hits'];
		$data['rows'] = [];
		$data['searchParams'] = $searchParams;
		$data['date_from'] = $start_time;
		$data['date_to'] = $end_time;
		$data['total'] = $response['hits']['total'];

		foreach($hits as $hit)
		{
			$row = [];
			$row['id'] = $hit['_id'];
				$dt = new DateTime();
				$dt->setTimestamp( $hit['_source']['receivedAt']/1000 );
			$row['time'] = $dt;
			$row['processor'] = $hit['_source']['processorName'];
			$row['processorRequestId'] = $hit['_source']['processorRequestId'];
			$row['status'] = $hit['_source']['providerResp']['errorCode'];
			$row['initiator'] = $hit['_source']['initiator'];
			$row['amount'] = $hit['_source']['normalizedAmount'];
			$row['action'] = $hit['_source']['parameters']['afcAction'];
			$row['notifiedAt'] = @$hit['notifiedAt'];//optionally present	
			$row['afcTxnDate'] = new DateTime(@$hit['_source']['parameters']['afcTransactionDate']);
			$row['revenueAcntSettled'] = @$hit['_source']['parameters']['revenueAcntSettled'];
				$st = new DateTime();
				$st->setTimestamp( @$hit['_source']['parameters']['revenueAcntSettledAt']/1000 );
			$row['revenueAcntSettledAt'] = $st;

			$data['rows'][] = $row;
		}
		

		return $data;
	}

	public function getDashboard()
	{
		
		$data = [];
		return $data;
	}


}