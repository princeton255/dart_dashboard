<?php

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function renderView($view, $data = NULL)
    {
    	$this->load->view('template/header');
    	$this->load->view('template/menu');
    	$this->load->view($view, $data);
    	$this->load->view('template/footer');
    }
}