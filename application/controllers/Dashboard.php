<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Default method
	 */
	public function index()
	{
		$this->load->model('Transactions');
		$data = $this->Transactions->getDashboard();
		$this->renderView('dashboard/index', $data);
	}

	public function topupTransactions()
	{
		if(isset($_GET['datefrom'])) {
			$limit = 10000;
		} else {
			$limit = 25;
		}
		
		$start_time = filter_input(INPUT_GET, 'datefrom');
		$end_time = filter_input(INPUT_GET, 'dateto');

		$searchParams = [];
		$searchParams['processorName'] = filter_input(INPUT_GET, 'processor');
		$searchParams['parameters.TrustAcctSettled'] = filter_input(INPUT_GET, 'TrustAcctSettled');
		$searchParams['processorRequestId'] = filter_input(INPUT_GET, 'txn_id');
		$searchParams['status'] = filter_input(INPUT_GET, 'status');

		$this->load->model('Transactions');
		$data = $this->Transactions->getTopup($limit, $start_time, $end_time , $searchParams);
		$this->renderView('dashboard/topup', $data);
	}

	public function usageTransactions()
	{
		if(isset($_GET['datefrom'])) {
			$limit = 10000;
		} else {
			$limit = 25;
		}
		
		$start_time = filter_input(INPUT_GET, 'datefrom');
		$end_time = filter_input(INPUT_GET, 'dateto');

		$searchParams = [];
		$searchParams['parameters.afcAction'] = filter_input(INPUT_GET, 'afcAction');
		$searchParams['parameters.RevenueAcctSettled'] = filter_input(INPUT_GET, 'RevenueAcctSettled');
		
		$this->load->model('Transactions');
		$data = $this->Transactions->getUsage($limit, $start_time, $end_time, $searchParams);
		$this->renderView('dashboard/usage', $data);
	}
}
