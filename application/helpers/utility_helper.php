<?php

function asset_url(){
   return base_url().'assets/';
}

function exchange_json($url, $json)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  0);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	$contents = curl_exec($ch);
	if(curl_exec($ch) === false)
	{
	  return false;
	}

	$data = json_decode($contents, true);

	return $data;
	
}