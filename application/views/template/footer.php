<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- jQuery -->
 <script src="<?= asset_url() ?>js/jquery.js"></script>
 

 <!-- Bootstrap Core JavaScript -->
 <script src="<?= asset_url() ?>js/bootstrap.min.js"></script>

 <!--Datatables Javascript -->
 <script src="<?= asset_url() ?>js/datatables.min.js"></script>

 <!-- Datepicker Javascript -->
 <script type="text/javascript" src="<?= asset_url() ?>js/datepicker.js"></script>

 <!-- Table to excel Javascript -->
 <script type="text/javascript" src="<?= asset_url() ?>js/table2excel.js"></script>

 <!-- Custom Javascript -->
 <script src="<?= asset_url() ?>js/custom.js"></script>

</body>

</html>
