<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?= base_url() ?>dashboard/"><img src="<?= asset_url() ?>images/logo3.png" height ="25px" width="75px" /></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>
					<a href="<?= base_url() ?>dashboard/topupTransactions"><span class="glyphicon glyphicon-transfer"></span> Topup</a>
				</li>
				<li>
					<a href="<?= base_url() ?>dashboard/usageTransactions"><span class="glyphicon glyphicon-transfer"></span> Usage</a>
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>
