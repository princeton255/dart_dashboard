<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="col-lg-2">
</div>
<div class="col-lg-8">
  <div class="well">
    <h4 class="card-title">DART Dashboard</h4>
    <p class="card-text">Click on links above to view <a href="<?= base_url()?>dashboard/topupTransactions" ><span class="glyphicon glyphicon-transfer"></span> Card Topup</a> and <a href="<?= base_url()?>dashboard/usageTransactions" ><span class="glyphicon glyphicon-transfer"></span> Card Usage Transactions</a></p>
  </div>
</div>