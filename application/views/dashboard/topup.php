<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); //print_r($rows); die;
?><!-- Page Content -->
   <div class="fluid">

   	<!-- Introduction Row -->
   	<div class="row">
   		<div class="col-lg-12">
   			<h1 class="page-header">DART
   				<small>TOP UP transactions</small>
   			</h1>
   		</div>
   	</div>

      <div class="filter_wrapper">
          <form>
          <!-- input block:begin -->
            <div class="col-lg-12">
              <div class="col-lg-2">
                  <label>
                      Processors:
                  </label>
                  <select class="form-control inputs required" name="processor" >
                      <option value="*">All Processors</option>
                      <option value="mpesa" <?php if($searchParams['processorName'] == 'mpesa') echo 'selected' ?> >Mpesa</option>
                      <option value="nmb" <?php if($searchParams['processorName'] == 'nmb') echo 'selected' ?>>NMB Mobile</option>
                      <option value="VTPOS" <?php if($searchParams['processorName'] == 'VTPOS') echo 'selected' ?>>Visiontek Pos</option>
                      <option value="tigopesa" <?php if($searchParams['processorName'] == 'tigopesa') echo 'selected' ?>>Tigopesa</option>
                  </select>           
              </div>
              <div class="col-lg-2">
                  <label>
                      TrustAccntSettled:
                  </label>
                  <select class="form-control inputs required" name="TrustAcctSettled" >
                      <option value="*">All States</option>
                      <option value="1" <?php if($searchParams['parameters.TrustAcctSettled'] == '1') echo 'selected' ?> >true</option>
                      <option value="0" <?php if($searchParams['parameters.TrustAcctSettled'] == '0') echo 'selected' ?> >false</option>
                  </select>
              </div>
              
              <div class="col-lg-2">
                  <label>
                      Txn ID:
                  </label>
                  <input value="<?= @$searchParams['processorRequestId'] ?>" class="form-control inputs required" name="txn_id" type="text">
              </div>
              <div class="col-lg-2">
                  <label>
                      Status:
                  </label>
                  <select class="form-control inputs required" name="status" >
                      <option value="*">All States</option>
                      <option value="SUCCESSFUL" <?php if($searchParams['status'] == 'SUCCESSFUL') echo 'selected' ?> >SUCCESS</option>
                      <option value="FAILED" <?php if($searchParams['status'] == 'FAILED') echo 'selected' ?> >FAILED</option>
                  </select>
              </div>
              <div class="col-lg-2">
                  <label>
                     Date From:
                  </label>
                  <input value="<?php echo @$date_from?>" class="form-control inputs required datepicker" name="datefrom" type="text">
              </div>
              <div class="col-lg-2">
                  <label>
                     Date To:
                  </label>
                  <input value="<?php echo @$date_to?>" class="form-control inputs required datepicker" name="dateto" type="text">
              </div>

            <!-- input block:end -->
            </div>


            <div class="col-lg-12" style="padding-top: 10px; padding-bottom: 10px;">
               
              <div class="col-lg-1">
                  <button class="btn btn-primary form-control"><i class="fa fa-search"></i> Search</button>
              </div>
              <div class="col-lg-1">
                  <div class="btn btn-success form-control excel_export"><i class="fa fa-table"></i> Export</div>
              </div>
            </div>

          </form>
      </div>

   <div col-lg-12>
  <h3>
    <?= $total ?> rows matched.

  </h3>
  <br/>
   	<table id="data_table" class="table table-hover">
   		<tr>
   			<th>Time</th>
   			<th>Amount</th>
   			<th>Processor</th>
        <th>Transaction Id</th>
        <th>Status</th>
   			<th>Initiator</th>
   			<th>TrustAccntSettled</th>
   			<th>TimeSettled</th>
   		</tr>
   	<?php if(empty($rows)): ?>
   		<tr>
   			<td colspan="6">No data found</td>
   		</tr>
   	<?php else: ?>
	   	<?php foreach($rows as $row): ?>
	   		<tr>
	   			<td><?= $row['time']->format("Y-m-d H:i:s") ?></td>
	   			<td><?= number_format($row['amount']) ?></td>
	   			<td><?= $row['processor'] ?></td>
               <td><?= $row['processorRequestId'] ?></td>
               <td><?= $row['status'] ?></td>
	   			<td><?= $row['initiator'] ?></td>
	   			<td><?= ($row['trustAcntSettledAt'])? 'true': 'false'; ?></td>
	   			<td><?= $row['trustAcntSettledAt'] ?></td>
	   		</tr>
	   	<?php endforeach; ?>
	<?php endif; ?>
   	</table>

    <p style="text-align:center">These are the first 25 rows. Use search options for more data</p>
   	<hr>
   </div>

   	<!-- Footer -->
   	<footer>
   		<div class="row">
   			<div class="col-lg-12">
   				<p style="text-align:center">Copyright &copy; Maxcom Africa Ltd</p>
   			</div>
   			<!-- /.col-lg-12 -->
   		</div>
   		<!-- /.row -->
   	</footer>

   </div>
   <!-- /.container -->