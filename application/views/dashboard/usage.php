<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- Page Content -->
   <div class="fluid">

   	<!-- Introduction Row -->
   	<div class="row">
   		<div class="col-lg-12">
   			<h1 class="page-header">DART
   				<small>USAGE transactions</small>
   			</h1>
   		</div>
   	</div>

      <div class="filter_wrapper" style="padding-bottom: 15px;">
          <form>
          <!-- input block:begin -->
            <div class="col-lg-12">
              <div class="col-lg-2">
                  <label>
                      Action:
                  </label>
                  <select class="form-control inputs required" name="afcAction" >
                      <option value="*">All</option>
                      <option value="CheckIn" <?php if($searchParams['parameters.afcAction'] == 'CheckIn') echo 'selected' ?> >CheckIn</option>
                      <option value="CheckOut" <?php if($searchParams['parameters.afcAction'] == 'CheckOut') echo 'selected' ?> >CheckOut</option>
                  </select>           
              </div>
              <div class="col-lg-2">
                  <label>
                      RevenueAccntSettled:
                  </label>
                  <select class="form-control inputs required" name="RevenueAcctSettled" >
                      <option value="*">All States</option>
                      <option value="1" <?php if($searchParams['parameters.RevenueAcctSettled'] == '1') echo 'selected' ?>>true</option>
                      <option value="0" <?php if($searchParams['parameters.RevenueAcctSettled'] == '0') echo 'selected' ?> >false</option>
                  </select>
              </div>
              
              <div class="col-lg-2">
                  <label>
                     Date From:
                  </label>
                  <input value="<?php echo @$date_from?>" class="form-control inputs required datepicker" name="datefrom" type="text">
              </div>
              <div class="col-lg-2">
                  <label>
                     Date To:
                  </label>
                  <input value="<?php echo @$date_to?>" class="form-control inputs required datepicker" name="dateto" type="text">
              </div>

              
            <!-- input block:end -->
            </div>

            <div class="col-lg-12" style="padding-top: 10px; padding-bottom: 10px;">
               
              <div class="col-lg-1">
                  <button class="btn btn-primary form-control"><i class="fa fa-search"></i> Search</button>
              </div>
              <div class="col-lg-1">
                  <div class="btn btn-success form-control excel_export"><i class="fa fa-table"></i> Export</div>
              </div>
            </div>
            
            <br/>
          </form>
      </div>
<div class="col-lg-12">
  <h3>
    <?= $total ?> rows matched.
  </h3>
   	<table id="data_table" class="table table-hover">
   		<tr>
   			<th>Time</th>
   			<th>Amount</th>
   			<th>Action</th>
        <th>Transaction Id</th>
   			<th>AFC Transaction Date</th>
   			<th>RevenueAccntSettled</th>
   			<th>TimeSettled</th>
   		</tr>
   	<?php if(empty($rows)): ?>
   		<tr>
   			<td colspan="6">No data found</td>
   		</tr>
   	<?php else: ?>
	   	<?php foreach($rows as $row): ?>
	   		<tr>
	   			<td><?= $row['time']->format("Y-m-d H:i:s") ?></td>
	   			<td><?= number_format($row['amount']) ?></td>
	   			<td><?= $row['action'] ?></td>
          <td><?= $row['processorRequestId'] ?></td>
	   			<td><?= $row['afcTxnDate']->format("Y-m-d H:i:s") ?></td>
	   			<td><?= ($row['revenueAcntSettled'])? 'true' : 'false' ?></td>
	   			<td><?= $row['revenueAcntSettledAt']->format("Y-m-d H:i:s") ?></td>
	   		</tr>
	   	<?php endforeach; ?>
	<?php endif; ?>
   	</table>

    <p style="text-align:center">These are the first 25 rows. Use search options for more data</p>
   	<hr>
</div>

   	<!-- Footer -->
   	<footer>
   		<div class="row">
   			<div class="col-lg-12">
   				<p style="text-align:center">Copyright &copy; Maxcom Africa Ltd</p>
   			</div>
   			<!-- /.col-lg-12 -->
   		</div>
   		<!-- /.row -->
   	</footer>

   </div>
   <!-- /.container -->